#!/usr/bin/env bash

python3 manage.py migrate --no-input
python3 manage.py collectstatic --no-input
gunicorn currency.wsgi:application -t 120 -w 2 -b 0.0.0.0:8000
