<h1>Currency Exchange Rate</h1>
<h3 align=center>First step</h3>
<b>Download project:</b>
<p>git clone https://HabibiDev@bitbucket.org/anton_bilitskyi/currency_exchange_rates.git</p>
<h3 align=center>Second step</h3>
<b>Install Docker, Docker-Compose:</b>

<a> https://docs.docker.com/engine/install/ </a>

<p>Create environments "container.env", for example "example_container.env"</p>

<h3 align=center>Third step</h3>

<b>Build and run docker images</b>
<p>From main folder "currency_exchange_rates/"</p>
<p>docker-compose up --build</p>

<b>If you want to create superuser</b>
<p>In another terminal window from dir "currency_exchange_rates/":</p>
<code>./m createsuperuser.</code>
<p>Write username, email(if you don't need just press 'Enter'), password.</p>

<b>If you want to run tests</b>
<p>In another terminal window from dir "currency_exchange_rates/":</p>
<code>./test</code>
