from rest_framework import generics

from currencies.models import TargetCurrencies
from currencies.serializers import TargetCurrenciesSerializerWriteOnly, TargetCurrenciesSerializerReadOnly


class TargetCurrenciesListCreateAPIView(generics.ListCreateAPIView):
    write_serializer_class = TargetCurrenciesSerializerWriteOnly
    read_serializer_class = TargetCurrenciesSerializerReadOnly
    queryset = TargetCurrencies.objects.all()

    def get_serializer_class(self):
        if self.request.method == "POST":
            return self.write_serializer_class
        return self.read_serializer_class
