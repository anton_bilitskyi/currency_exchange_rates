from django.urls import path

from currencies import views

app_name = "currencies"

urlpatterns = [
    path('exchange_rates', views.TargetCurrenciesListCreateAPIView.as_view(), name="exchange_rates"),
]
