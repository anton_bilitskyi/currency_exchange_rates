from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Currency, ExchangeRate, TargetCurrencies
from .services import currency_service_api_mapping


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ["id", "name", "short_name"]


class TargetCurrenciesSerializerReadOnly(serializers.ModelSerializer):
    base = CurrencySerializer()
    target = CurrencySerializer()
    exchange_rate = serializers.SerializerMethodField(read_only=True)
    
    @staticmethod
    def get_exchange_rate(obj):
        exchange_rate = ExchangeRate.objects.filter(target_currencies=obj).last()
        return exchange_rate.value

    class Meta:
        model = TargetCurrencies
        fields = ["id", "base", "target", "exchange_rate"]


class TargetCurrenciesSerializerWriteOnly(serializers.ModelSerializer):
    base = serializers.CharField()
    target = serializers.CharField()

    def validate(self, data):
        base = data.get("base").lower()
        target = data.get("target").lower()

        if not base:
            raise ValidationError('Base currency field is required.')

        if not target:
            raise ValidationError('Target currency field is required.')

        if base == target:
            raise ValidationError('Base and Target currencies should be different')

        exchange_rate = None
        for _, service in currency_service_api_mapping.items():
            exchange_rate = service(base, target).get_exchange_rate()
            if exchange_rate:
                break

        if not exchange_rate:
            raise ValidationError(f"Exchange rate of {data.get('base')}/{data.get('target')} doesn't exist")

        data.update({
            "exchange_rate": exchange_rate,
            "base": base,
            "target": target,
        })
        return data

    def create(self, validated_data):
        exchange_rate = validated_data.pop("exchange_rate")
        base, _ = Currency.objects.get_or_create(short_name=validated_data["base"])
        target, _ = Currency.objects.get_or_create(short_name=validated_data["target"])
        instance, is_created = TargetCurrencies.objects.get_or_create(base=base, target=target)
        if is_created:
            ExchangeRate.objects.create(target_currencies=instance, value=exchange_rate)
        return instance

    class Meta:
        model = TargetCurrencies
        fields = ["id", "base", "target"]
