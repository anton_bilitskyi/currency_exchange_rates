from decimal import Decimal

import requests

from currencies.constants import CRYPTONATOR_API_SERVICE


class BaseCurrencyServiceApi:
    """
    Base interface to work with currency exchange rates service api
    """
    NAME = 'base'
    BASE_URL = None

    def __init__(self, first_currency: str, second_currency: str):
        self.first_currency = first_currency
        self.second_currency = second_currency

    def is_success_response(self, response) -> bool:
        raise NotImplementedError("You should implement this method")

    def get_exchange_rate(self) -> None or Decimal:
        raise NotImplementedError("You should implement this method")


class CryptonatorCurrencyServiceApi(BaseCurrencyServiceApi):
    """
    Interface to work with Cryptonator service api
    """
    NAME = CRYPTONATOR_API_SERVICE
    BASE_URL = "https://api.cryptonator.com/api/ticker/"

    def is_success_response(self, response) -> bool:
        data = response.json()
        return data["success"]

    def get_exchange_rate(self) -> None or Decimal:
        response = requests.get(
            self.BASE_URL + f"{self.first_currency}-{self.second_currency}",
            headers={
                'Content-Type': 'application/json',
                'User-Agent': "Mozilla/5.0"
            }
        )
        if self.is_success_response(response):
            data = response.json()
            return Decimal(data["ticker"]["price"])
        return None


currency_service_api_mapping = {service.NAME: service for service in BaseCurrencyServiceApi.__subclasses__()}
