from currencies.models import ExchangeRate, TargetCurrencies
from currencies.services import currency_service_api_mapping
from currency.celery import app


@app.task(bind=True)
def update_exchange_rates(*args, **kwargs):
    service_name = kwargs.get("service_name")
    currency_service_api = currency_service_api_mapping.get(service_name)
    if not currency_service_api:
        raise NotImplementedError(f"Interface for {service_name} is not implemented")
    target_currencies = TargetCurrencies.objects.all()
    for target_currency in target_currencies:
        updated_exchange_rate = currency_service_api(
            target_currency.base.short_name, target_currency.target.short_name
        ).get_exchange_rate()
        if updated_exchange_rate:
            ExchangeRate.objects.create(target_currencies=target_currency, value=updated_exchange_rate)
