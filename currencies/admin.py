from django.contrib import admin

from currencies.models import Currency, ExchangeRate, TargetCurrencies

admin.site.register(Currency)
admin.site.register(ExchangeRate)
admin.site.register(TargetCurrencies)
