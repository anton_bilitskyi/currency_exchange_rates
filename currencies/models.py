from django.db import models


class Currency(models.Model):
    name = models.CharField(max_length=10, null=True)
    short_name = models.CharField(max_length=4, unique=True, null=False)

    def __str__(self):
        return f"{self.short_name} - {self.name}"


class TargetCurrencies(models.Model):
    base = models.ForeignKey(Currency, on_delete=models.CASCADE, null=False, related_name="base_currencies")
    target = models.ForeignKey(Currency, on_delete=models.CASCADE, null=False, related_name="target_currencies")

    class Meta:
        unique_together = ["base", "target"]

    def __str__(self):
        return f"{self.base.short_name}/{self.target.short_name}"


class ExchangeRate(models.Model):
    target_currencies = models.ForeignKey(TargetCurrencies, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=20, decimal_places=10, null=False)
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.target_currencies}: {self.value}"
