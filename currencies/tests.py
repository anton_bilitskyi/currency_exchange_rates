from decimal import Decimal
from unittest import mock

from rest_framework.reverse import reverse as api_reverse
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from rest_framework.test import APITestCase as TestCase

from currencies.constants import CRYPTONATOR_API_SERVICE
from currencies.models import Currency
from currencies.services import CryptonatorCurrencyServiceApi
from currencies.tasks import update_exchange_rates, TargetCurrencies, ExchangeRate


class CryptonatorCurrencyServiceApiTestCase(TestCase):

    def test_get_exchange_rate_successfully(self):
        base_currency = 'USD'
        target_currency = 'RUR'
        cryptonator = CryptonatorCurrencyServiceApi(base_currency, target_currency)
        result = cryptonator.get_exchange_rate()
        self.assertIsNotNone(result)
        self.assertIsInstance(result, Decimal)

    def test_get_exchange_rate_failed(self):
        base_currency = 'wrong_currency'
        target_currency = 'RUR'
        cryptonator = CryptonatorCurrencyServiceApi(base_currency, target_currency)
        result = cryptonator.get_exchange_rate()
        self.assertIsNone(result)


class UpdateExchageRatesTestCase(TestCase):

    def test_update_exchange_rate_with_wrong_service_name(self):
        kwargs = {"service_name": "wrong_service_name"}
        with self.assertRaises(NotImplementedError):
            update_exchange_rates(**kwargs)

    @mock.patch.object(ExchangeRate.objects, "create")
    @mock.patch.object(CryptonatorCurrencyServiceApi, "get_exchange_rate")
    @mock.patch.object(TargetCurrencies.objects, "all")
    def test_update_exchange_rate_with_service_name_cryptonator_not_none_result(self, mock_target_currencies,
                                                                                mock_get_exchange_rate,
                                                                                mock_exchange_rate):
        mock_get_exchange_rate.return_value = "Not None"
        count_target_currencies = 3
        mock_target_currencies.return_value = [mock.MagicMock(
            base=mock.Mock(short_name=f"base_currency_{i}"), target=mock.Mock(short_name=f"target_currency_{i}")
        ) for i in range(count_target_currencies)]
        kwargs = {"service_name": CRYPTONATOR_API_SERVICE}
        update_exchange_rates(**kwargs)
        self.assertEqual(mock_get_exchange_rate.call_count, count_target_currencies)
        self.assertEqual(mock_exchange_rate.call_count, count_target_currencies)

    @mock.patch.object(ExchangeRate.objects, "create")
    @mock.patch.object(CryptonatorCurrencyServiceApi, "get_exchange_rate")
    @mock.patch.object(TargetCurrencies.objects, "all")
    def test_update_exchange_rate_with_service_name_cryptonator_with_none_result(self, mock_target_currencies,
                                                                                 mock_get_exchange_rate,
                                                                                 mock_exchange_rate):
        mock_get_exchange_rate.return_value = None
        count_target_currencies = 3
        mock_target_currencies.return_value = [mock.MagicMock(
            base=mock.Mock(short_name=f"base_currency_{i}"), target=mock.Mock(short_name=f"target_currency_{i}")
        ) for i in range(count_target_currencies)]
        kwargs = {"service_name": CRYPTONATOR_API_SERVICE}
        update_exchange_rates(**kwargs)
        self.assertEqual(mock_get_exchange_rate.call_count, count_target_currencies)
        self.assertEqual(mock_exchange_rate.call_count, 0)


class TargetCurrenciesTestCase(TestCase):

    def setUp(self) -> None:
        self.usd_currency = Currency.objects.create(short_name="usd")
        self.rur_currency = Currency.objects.create(short_name="rur")
        self.eur_currency = Currency.objects.create(short_name="eur")

        self.target_currencies_usd_rur = TargetCurrencies.objects.create(base=self.usd_currency,
                                                                         target=self.rur_currency)
        self.target_currencies_eur_rur = TargetCurrencies.objects.create(base=self.eur_currency,
                                                                         target=self.rur_currency)
        self.target_currencies_usd_eur = TargetCurrencies.objects.create(base=self.usd_currency,
                                                                         target=self.eur_currency)

        self.target_currencies = [self.target_currencies_usd_rur,
                                  self.target_currencies_eur_rur,
                                  self.target_currencies_usd_eur]

        for target_currency in self.target_currencies:
            ExchangeRate.objects.create(target_currencies=target_currency, value=70.12345678)

    def test_get_exchange_rate_list(self):
        url = api_reverse(
            'currencies:exchange_rates',
        )
        data = {}
        resp = self.client.get(url, data, format='json')
        self.assertEqual(resp.status_code, HTTP_200_OK)
        self.assertEqual(len(resp.data), TargetCurrencies.objects.count())

    @mock.patch.object(CryptonatorCurrencyServiceApi, "get_exchange_rate")
    def test_add_target_currencies(self, mock_get_exchange_rate):
        mock_get_exchange_rate.return_value = Decimal(55)
        count_target_currencies = TargetCurrencies.objects.count()
        count_currencies = Currency.objects.count()
        url = api_reverse(
            'currencies:exchange_rates',
        )
        data = {"base": "BTC", "target": "EUR"}
        resp = self.client.post(url, data, format='json')
        self.assertEqual(resp.status_code, HTTP_201_CREATED)
        self.assertEqual(TargetCurrencies.objects.count(), count_target_currencies + 1)
        self.assertEqual(Currency.objects.count(), count_currencies + 1)
        mock_get_exchange_rate.assert_called_once_with()

    @mock.patch.object(CryptonatorCurrencyServiceApi, "get_exchange_rate")
    def test_add_target_currencies_failed(self, mock_get_exchange_rate):
        mock_get_exchange_rate.return_value = None
        test_data = [
            {
                "base": None,
                "target": "usd"
            },
            {
                "base": "usd",
                "target": None
            },
            {
                "base": "usd",
                "target": "usd"
            },
            {
                "base": "usd",
                "target": "rur"
            },
        ]
        for data in test_data:
            url = api_reverse(
                'currencies:exchange_rates',
            )
            resp = self.client.post(url, data, format='json')
            self.assertEqual(resp.status_code, HTTP_400_BAD_REQUEST)
