from currency.settings.common import *

ALLOWED_HOSTS = ["*"]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'test_db',
    }
}

CELERY_RESULT_BACKEND = "redis://redis:6379/test"
BROKER_URL = "redis://redis:6379/test"
