import os
from datetime import timedelta

from celery import Celery
from django.conf import settings

# set the default Django settings module for the 'celery' program.
from currencies.constants import CRYPTONATOR_API_SERVICE

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'currency.settings.local_settings')

app = Celery('currency')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(settings.INSTALLED_APPS)

app.conf.beat_schedule = {
    "update_exchange_rate_using_cryptonator": {
        "task": "currencies.tasks.update_exchange_rates",
        "schedule": timedelta(seconds=10),
        "kwargs": {"service_name": CRYPTONATOR_API_SERVICE}
    },
}
